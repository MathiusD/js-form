<?php
    $file = "./base.json";
    $db = fopen($file, "r");
    $json = fread($db, filesize($file));
    fclose($db);
    $base = json_decode($json, true);
    $data = array();
    if(isset($_GET['term']))
    {
        foreach ($base['style'] as $style)
        {
            if(preg_match('/^('.$_GET['term'].')/i', $style) != NULL)
            {
                array_push($data, $style);
            }
        }
    }
    if(isset($_GET['verif']))
    {
        $exist = false;
        foreach ($base['groupe'] as $groupe)
        {
            if(strcasecmp ($_GET['verif'], $groupe) == 0)
            {
                $exist = true;
            }
        }
        $data['exist'] = $exist;
    }
    echo json_encode($data);
?>