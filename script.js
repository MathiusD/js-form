function own_script()
{
    $('#input_auto').autocomplete({source:"api.php"});
    $('#verif_warn').hide();
    $('#input_verif').change(
        function() {
            $.getJSON('api.php?verif='+$('#input_verif').val(), function( data ) {
                $.each(data, function( key, val ) {
                    if(key == "exist")
                    {
                        $('#verif_warn').toggle(val);
                    }
                });
            });
        }
    );
}